<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestApiFrontPageController extends Controller
{
    public function front()
    {
        return view('front');
    }
}
