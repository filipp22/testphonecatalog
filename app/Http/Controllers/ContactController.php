<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Contact;
use Illuminate\Http\Response;


class ContactController extends Controller
{
    private $maxTimeIntervalHours = 24;

    public function show()
    {

        $result = false;

        $phone = Contact::NormalizePhone(\request()->get('phone'));

        if ($phone) {
            $res = Contact::where('phone', $phone)->get();
            if (count($res))
                $result = $res;
        }

        return json_encode(['result' => $result], JSON_UNESCAPED_UNICODE);
    }

    public function create(): string
    {

        $countCreatedItems = 0;

        $data = json_decode(\Request::getContent(), true);

        if ($sourceId = intval($data['sourceid']) && $items = $data['items']) {
            $data = [];
            $phoneListCheck = [];
            foreach ($items as $item) {
                if ($phone = Contact::NormalizePhone($item["phone"])) {
                    $data[] = [
                        'source_id' => $sourceId,
                        'name' => $item['name'],
                        'phone' => $phone,
                        'email' => $item['email'],
                        'created_at' => Carbon::now()
                    ];

                    $phoneListCheck[] = $phone;
                }

            }

            $whiteSheetPhone = Contact::select('phone')
                ->whereIn('phone', $phoneListCheck)
                ->where('created_at', '>=', Carbon::now()->subHours($this->maxTimeIntervalHours)->toDateTimeString())
                ->pluck('phone')
                ->toArray();

            $insert = [];

            foreach ($data as $value) {
                if (!in_array($value['phone'], $whiteSheetPhone))
                    $insert[] = $value;
            }

            if (count($insert)) {
                if (Contact::insert($insert)) {
                    $countCreatedItems = count($insert);
                }
            }

        }

        return json_encode([ 'count' => $countCreatedItems]);

    }
}
