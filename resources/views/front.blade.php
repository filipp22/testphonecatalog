<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Test</title>


    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/sticky-footer-navbar.css" rel="stylesheet">
</head>

<body>

<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/">Contact Base</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
            </ul>
            <form id="search" action="{{route('show')}}" class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" name="phone" type="text" placeholder="Search phone" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
</header>

<!-- Begin page content -->
<main role="main" class="container">
    <form id="add" action="{{route('create')}}" method="post">
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Add phones</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="6">
            		{
			"sourceid": 1,
			"items": [{
				"name": "Анна",
				"phone": 9001234453,
				"email": "mail1@gmail.com"
			}, {
				"name": "Иван",
				"phone": "+79001234123",
				"email": "mail2@gmail.com"
			}]
		}

        </textarea>
    </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <div class="app">
        <p>Result:</p>
        <div id="result">
            <code>null</code>

        </div>
    </div>
</main>

<footer class="footer">
    <div class="container">
        <span class="text-muted">test</span>
    </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="/js/main.js"></script>

</body>
</html>
