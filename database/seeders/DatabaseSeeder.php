<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Models\Contact;

class DatabaseSeeder extends Seeder
{

    public function run(): void
    {
        for ($i = 0; $i < 1000; $i++)
            Contact::create([
                'source_id' => Arr::random([1, 2]),
                'phone' => (int)rand(1000000000, 9999999999),
                'name' => Arr::random(["Анна", "Юра", "Иван", "Александр"]),
                'email' => Str::random(32) . '@mail.ru',

            ]);

        $this->command->info('Contact table seeded!');
    }
}
